# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 19:14:09 2020

@author: SHIRISH
"""


import cv2 
import time 
import numpy as np
import Convolution
import Test_Convolution
import sys
import matplotlib.pyplot as plt

x = 0
count = 1
flag = 0
global original_img
global changed_img
global X_derivative 
global y_derivative
global image_grayscale




if len(sys.argv) == 2:
    filename = sys.argv[1]
    flag = 1

else:
    cap = cv2.VideoCapture(0)
    ret,img = cap.read()
    flag=-1


global img_grayscale

#To get the derivatives using central derivatives method
delta_x = np.array([[-1,0,1],[-1,0,1],[-1,0,1]]).astype('float32')
delta_y = np.array([[1,1,1],[0,0,0],[-1,-1,-1]]).astype('float32')




def ManualConvolution(winName):
    #print(winName)
    kernel = np.ones((winName,winName),np.float32)/(winName*winName)
    temp = np.zeros(image_grayscale.shape).astype('float32')
    input1 = np.array(image_grayscale).astype('float32')
    output_conv = Convolution.convolve(input1,temp,kernel)
    output_conv = np.array(output_conv).astype('uint8')
    cv2.imshow('I', output_conv)
    
def SlidingWindowbar(winName):
    kernel = np.ones((winName,winName),np.float32)/(winName*winName)
    output_img = cv2.filter2D(image_grayscale, -1, kernel)
    cv2.imshow('I',output_img)

def Sampling_withSmoothing(Img):
    n = 5
    kernel = np.ones((n,n),np.float32)/(n*n)
    img_conv = cv2.filter2D(Img, -1, kernel)
    #print(img_conv)
    cv2.imshow("Downsampled Image with Smoothing", img_conv)    

def get_derivative_x(Img):
    temp = np.zeros(Img.shape).astype('float32')
    input1 = np.array(Img).astype('float32')
    output_conv = Convolution.convolve(input1,temp,delta_x)/255
    output_conv = np.array(output_conv).astype('uint8')
    return output_conv
    

def get_derivative_Y(Img):
    temp = np.zeros(Img.shape).astype('float32')
    input1 = np.array(Img).astype('float32')
    output_conv = Convolution.convolve(input1,temp,delta_y)/255
    output_conv = np.array(output_conv).astype('uint8')
    return output_conv


def get_derivative_x_without_normalization(Img):
    temp = np.zeros(Img.shape).astype('float32')
    input1 = np.array(Img).astype('float32')
    output_conv = Convolution.convolve(input1, temp, delta_x)
    output_conv = np.array(output_conv).astype('uint8')
    return output_conv


def get_derivative_Y_without_normalization(Img):
    temp = np.zeros(Img.shape).astype('float32')
    input1 = np.array(Img).astype('float32')
    output_conv = Convolution.convolve(input1, temp, delta_y)
    output_conv = np.array(output_conv).astype('uint8')
    return output_conv



    
def getMagnitude(Img):
    X_derivative = get_derivative_x(Img)
    y_derivative = get_derivative_Y(Img)
    magnitude = np.zeros(X_derivative.shape).astype('uint8')
    
    for i in range(magnitude.shape[0]):
        for j in range(magnitude.shape[1]):
            magnitude[i][j] = np.sqrt((X_derivative[i][j] ** 2 + y_derivative[i][j] ** 2)) 
    
    
    return magnitude

def cycleColors(img,count):
    if count == 1:
        img[:,:,0]=0
        img[:,:,1] = 0

    if count == 2:
        img[:, :, 0] = 0
        img[:, :, 2] = 0

    if count == 3:
        img[:, :, 1] = 0
        img[:, :, 2] = 0

    cv2.imshow("ColorPalate",img)

def RotateImage(angle):
    rad = angle * np.pi/180
    rows = image_grayscale.shape[0]
    cols = image_grayscale.shape[1]
    m = cv2.getRotationMatrix2D((cols/2,rows/2),rad,1)
    det = cv2.warpAffine(original_img,m,(cols,rows))
    if len(det.shape) == 3:
        det = cv2.cvtColor(det, cv2.COLOR_BGR2GRAY)
    cv2.imshow("Rotated_Matrix",det)

def PlotGradientVectors(n):
    output_gradients_x = []
    output_gradients_y = []
    k = 50
    i = 0
    j = 0
    if n < original_img.shape[0] and n < original_img.shape[1]:
        for i in range(n):
            for j in range(n):
                if i == 0 or j == 0:
                    output_gradients_x.append(X_derivative[i][j])
                    output_gradients_y.append(y_derivative[i][j])
                else:
                    output_gradients_x.append(X_derivative[i][j+k])
                    output_gradients_y.append(y_derivative[i][j+k])
    #print(X_derivative)
    '''
    print(len(output_gradients_x))
    print(len(output_gradients_y))
    '''
    plt.plot(output_gradients_x,output_gradients_y)
    plt.xlabel("X_derivative")
    plt.ylabel("Y_derivative")
    plt.savefig("GradientVectors.png")
    image = cv2.imread("GradientVectors.png")
    cv2.imshow("Gradient_vectors",image)



   
# Open the camera 
#cap = cv2.VideoCapture(0) 
if flag == -1:
    original_img = img
    temp = original_img    

else:
    original_img = cv2.imread(filename)
    temp = original_img
  
while True: 
      
    # Read and display each frame
    if flag == -1:
        ret, img = cap.read() 
        original_img = img
        cv2.imshow('a', original_img)
        
    
    elif flag == 1:
        cv2.imshow('a', original_img)
        
    elif flag == 2:
        original_img = temp
        cv2.imshow('a',original_img)
  
    # check for the key pressed 
    k = cv2.waitKey(125) 
  
    # set the key for the countdown 
    # to begin. Here we set q 
    # if key pressed is q 
    if k == ord('g'):
        prev = time.time()
        if len(original_img.shape) == 3:
            image_grayscale = cv2.cvtColor(original_img, cv2.COLOR_BGR2GRAY)
        cv2.imshow("GrayScale", image_grayscale)
        
    elif k == ord('G'):
        prev = time.time()
        if len(original_img.shape) == 3:
            image_grayscale = (0.11 * original_img[:,:,0] + 0.59 * original_img[:,:,1] + 0.3 * original_img[:,:,2])
            image_grayscale = np.array(image_grayscale).astype('uint8')
        cv2.imshow("Grayscale", image_grayscale)
                
    elif k == ord('s'):
        prev = time.time()
        
        if len(original_img.shape) == 3:
            image_grayscale = cv2.cvtColor(original_img, cv2.COLOR_BGR2GRAY)
            
        winName='I'
        cv2.imshow(winName,original_img)
        cv2.createTrackbar('s', winName, 1, 255, SlidingWindowbar)


        
    elif k == ord('d'):
        sample_img = original_img
        factor = 2
        row_factor = int(sample_img.shape[0]/factor)
        column_factor = int(sample_img.shape[1]/factor)
        sample_img = cv2.resize(sample_img,(row_factor,column_factor))
        #print(row_factor)
        #print(column_factor)
        cv2.imshow("Down Sampling without Smoothing",sample_img)
        
    elif k == ord('D'):
        sample_img = original_img
        factor = 2
        row_factor = int(sample_img.shape[0]/factor)
        column_factor = int(sample_img.shape[1]/factor)
        sample_img = cv2.resize(sample_img,(row_factor,column_factor))
        Sampling_withSmoothing(sample_img)
        
    elif k == ord('S'):
        prev = time.time()
        if len(original_img.shape) == 3:
            image_grayscale = cv2.cvtColor(original_img, cv2.COLOR_BGR2GRAY)
        else:
            image_grayscale = original_img
        winName='I'
        cv2.imshow(winName,original_img)
        cv2.createTrackbar('s', winName, 1, 255,ManualConvolution)
        
    elif k == ord('x'):
        prev = time.time()
        if len(original_img.shape) == 3:
            original_img = cv2.cvtColor(original_img, cv2.COLOR_BGR2GRAY)
        
        original_img = get_derivative_x(original_img)
        cv2.imshow('a', original_img)
    elif k == ord('y'):
        prev = time.time()
        if len(original_img.shape) == 3:
            original_img = cv2.cvtColor(original_img, cv2.COLOR_BGR2GRAY)

        original_img = get_derivative_Y(original_img)
        cv2.imshow("a", original_img)
        
    
    #Code to display the magnitude of the gradients    
    elif k == ord('m'):
        prev = time.time()
        if len(original_img.shape) == 3:
            original_img = cv2.cvtColor(original_img, cv2.COLOR_BGR2GRAY)
        
        original_img = getMagnitude(original_img)
        cv2.imshow("a", original_img)
            
    elif k == ord('p'):
        prev = time.time()
        image_grayscale = np.copy(original_img)
        if len(image_grayscale.shape) == 3:
            image_grayscale = cv2.cvtColor(image_grayscale, cv2.COLOR_BGR2GRAY)
        window = "Image"
        X_derivative = get_derivative_x_without_normalization(image_grayscale)
        y_derivative = get_derivative_Y_without_normalization(image_grayscale)

        cv2.imshow(window,image_grayscale)
        cv2.createTrackbar('s', window, 1, 100, PlotGradientVectors)
        # image = cv2.imread("GradientVectors.png")
        # cv2.imshow("Gradient_vectors",image)

    elif k == ord('i'):
        original_img = temp
        cv2.imshow('a',original_img)

    elif k == ord('r'):
        image_grayscale = np.copy(original_img)
        if len(image_grayscale.shape) == 3:
            image_grayscale = cv2.cvtColor(image_grayscale, cv2.COLOR_BGR2GRAY)
        window="Rotated_Matrix"
        cv2.imshow(window, image_grayscale)
        cv2.createTrackbar('s', window, 0, 1000, RotateImage)

    elif k == ord('w'):
        cv2.imwrite("SavedImage.png",original_img)

    elif k == ord('h'):
        print("Welcome to the Interactive Image processing application menu")
        print("Press g or G to perform gray scale conversion \n Press s or S to perform smoothing using cv2 function or Cython function \n Press x or y to obtain the derivatives of the image \n Press R to rotate the image \n Press r to reset the image \n Press M to get the magnitude of the gradients \n Press P to plot the gradient vectors\nPress Esc button to quit\nPress w to save the processed image to disk")

    elif k == ord('c'):
        prev = time.time()

        if len(original_img.shape) == 3:
            if count == 1:
                image_grayscale = np.copy(original_img)
                image_grayscale[:, :, 0] = 0
                image_grayscale[:, :, 1] = 0

            if count == 2:
                image_grayscale = np.copy(original_img)
                image_grayscale[:, :, 0] = 0
                image_grayscale[:, :, 2] = 0

            if count == 3:
                image_grayscale = np.copy(original_img)
                image_grayscale[:, :, 1] = 0
                image_grayscale[:, :, 2] = 0

        else:
            print("Wrong type of image")

        cv2.imshow("ColorWindos",image_grayscale)
        if count >= 4:
            count = 0
        count += 1

        
  
    # Press Esc to exit 
    elif k == 27: 
        break
  
# close the camera
if flag == -1:
    cap.release() 
   
# close all the opened windows 
cv2.destroyAllWindows()



cimport cython
import numpy as np

#To increase computation speed of cython
@cython.boundscheck(False)
@cython.wraparound(False)
def convolve(float[:, ::1] image, float[:, ::1] output, float[:, ::1] kernel):
    cdef ssize_t image_height, image_width
    cdef ssize_t kernel_height, kernel_width, kernel_halfh, kernel_halfw
    cdef ssize_t min_x, max_x, min_y, max_y, x, y, u, v #To get the boundary values on either side of the image
    
    cdef float conv_value, temp

    image_height = image.shape[1] #Image height
    image_width = image.shape[0]  #Image width

    kernel_height = kernel.shape[1]
    kernel_halfh = kernel_height // 2 #Since using the formula for convolution
    kernel_width = kernel.shape[0]
    kernel_halfw = kernel_width // 2

    # Do convolution
    for x in range(image_width):
        for y in range(image_height):
            # Calculate The neighbouring pixel of the filter
            #Implicitly applying zero padding by selecting the borders zero
            min_x = max(0, x - kernel_halfw)
            max_x = min(image_width - 1, x + kernel_halfw)
            min_y = max(0, y - kernel_halfh)
            max_y = min(image_height - 1, y + kernel_halfh)

            # Convolve filter
            conv_value = 0
            conv_total = 0
            for u in range(min_x, max_x + 1): #Traversing kernel from left to write
                for v in range(min_y, max_y + 1): #Traversing Kernel from top to bottom
                    #Calculating the sum of the kernal elements and adding to the image element at u,v
                    temp = kernel[v - y + kernel_halfh, u - x + kernel_halfw]
                    conv_value += image[u,v] * temp


            output[x, y] = conv_value

            
            
    return np.asarray(output)
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 16:43:01 2020

@author: SHIRISH
"""


from distutils.core  import setup
from Cython.Build import cythonize

setup(name = 'Convolution_module',ext_modules = cythonize("Convolution.pyx"),
      )
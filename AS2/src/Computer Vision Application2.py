# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 22:08:13 2020

@author: SHIRISH
"""
import cv2 
#import time
import numpy as np
import Votes
import sys
import matplotlib.pyplot as plt


x = 0
count = 1
flag = 0
global original_img
theta =  list(range(0,180))

global image_grayscale
filename = 0
votes = {}



if len(sys.argv) == 2:
    filename = sys.argv[1]
    flag = 1

else:
    cap = cv2.VideoCapture(0)
    ret,img = cap.read()
    flag=-1


global img_grayscale


if flag == -1:
    original_img = img
    temp = original_img    

else:
    original_img = cv2.imread(filename)
    temp = original_img
    display_img = original_img


def nothing(x):
    pass

cv2.namedWindow('Image')
cv2.createTrackbar('Internal_Threshold','Image', 0, 100, nothing)
cv2.createTrackbar('External_Threshold','Image', 0, 100, nothing)
cv2.createTrackbar('houghThreshold','Image',0,500,nothing)
cv2.createTrackbar('bin_size','Image',0,500,nothing)

def show_lines(threshold,votes):
    if filename == 0:
        out_img = img_grayscale
    else:
        out_img = cv2.imread(filename)
    print(threshold)
    for keys in votes.keys():
        if votes[keys] > threshold:
            values = list(keys)
            a = np.cos(values[1])
            b = np.sin(values[1])
            # Getting the origin
            x0 = a * values[0]
            y0 = b * values[0]
            x1 = int(x0 + 1000 * (-b))
            y1 = int(y0 + 1000 * (a))
            x2 = int(x0 - 1000 * (-b))
            y2 = int(y0 - 1000 * (a))
            cv2.line(out_img, (x1, y1), (x2, y2), (0, 0, 255), 1)

        cv2.imshow("Image",out_img)

    



def Canny_edge_detection(img):
    ext_t = cv2.getTrackbarPos('Internal_Threshold', 'Image')
    in_t = cv2.getTrackbarPos('External_Threshold', 'Image')
    gauss_blur = cv2.GaussianBlur(img, (3, 3), 1)
    cann_img = cv2.Canny(gauss_blur, in_t, ext_t)
    return  cann_img
    
def hough_transform(img):
    if filename == 0:
        out_img = img
    else:
        out_img = cv2.imread(filename)
    
    ext_t = cv2.getTrackbarPos('Internal_Threshold','Image')
    in_t = cv2.getTrackbarPos('External_Threshold','Image')
    hough_threshold = cv2.getTrackbarPos('houghThreshold','Image')
    bin_size = cv2.getTrackbarPos('bin_size', 'Image')
    gauss_blur = cv2.GaussianBlur(img, (3, 3), 1)
    cann_img = cv2.Canny(gauss_blur, in_t, ext_t)
    cv2.imshow('Image',cann_img)
    cann_img = np.array(cann_img).astype('int')
    hough_votes = {}
    Votes.getVotes(cann_img,hough_votes,bin_size)
    #print(sorted(hough_votes))
    #Dispaying the lines unrefined one
    show_lines(hough_threshold,hough_votes)
    #refine_lines(hough_threshold,hough_votes)
    points = []

    for i in range(cann_img.shape[0]):
        for j in range(cann_img.shape[1]):
            if cann_img[i][j] != 0:
                points.append([i,j])

    for keys in hough_votes.keys():
        if hough_votes[keys] > hough_threshold:
            values = list(keys)
            line_coeff = []
            X_array = []
            y_array = []
            for p in points:
                if values[0] == int((p[0]*np.cos(values[1])+(p[1]*np.sin(values[1])))):
                    X_array.append(p[0])
                    y_array.append(p[1])

            X_array = np.array(X_array)
            X_array = X_array.reshape((X_array.shape[0],1))
            y_array = np.array(y_array)
            y_array = y_array.reshape((y_array.shape[0],1))
            #Refining the edge pixels
            sum_X = np.sum(X_array)
            sum_squares = np.sum(np.square(X_array))
            sum_X_y = np.dot(np.transpose(X_array),y_array)
            sum_y = np.sum(y_array)
            A = np.array([[sum_squares,sum_X],[sum_X,len(X_array)]])
            b = np.array([[sum_X_y,sum_y]])
            b = b.reshape((2,1))
            line_coeff.append(np.dot(np.linalg.inv(A),b))

            for l_coeff in line_coeff:
                for i in range(X_array.shape[0]):
                    y1 = int((l_coeff[0]*X_array[i]) + l_coeff[1])
                    y2 = int((l_coeff[0]*(1000 + X_array[i])) + l_coeff[1])
                    cv2.line(out_img, (int(X_array[i]), y1), (int(X_array[i]+1000), y2), (0, 0, 255), 1)


    cv2.imshow("RefinedLines",out_img)

while True: 
      
    # Read and display each frame
    if flag == -1:
        ret, img = cap.read() 
        original_img = img
        cv2.imshow('a', original_img)
        
    
    elif flag == 1:
        cv2.imshow('a', original_img)
        
    elif flag == 2:
        original_img = temp
        cv2.imshow('a',original_img)
  
    # check for the key pressed 
    k = cv2.waitKey(125) 
  
    # set the key for the countdown 
    # to begin. Here we set q 
    # if key pressed is q 
    if k == ord('c'):
        img_grayscale = cv2.cvtColor(original_img, cv2.COLOR_BGR2GRAY)
        cann_img= Canny_edge_detection(img_grayscale)
        cv2.imshow("Image",cann_img)

    elif k == ord('d'):
        img_grayscale = cv2.cvtColor(original_img, cv2.COLOR_BGR2GRAY)
        hough_transform(img_grayscale)
        #print(votes)

    elif k == ord('h'):
        print("Welcome the Line Detection using Hough Transform \n")
        print("1.Adjust the values in the trackbar first and then press \n 1.c to perform "
              "Canny edge detection  using the threshold provided in trackbar\n "
              "2.Press d to get print the lines detected in the image using hough transoform \n "
              "4. Press P to show the parameter space of the hough transform")
        
       

    elif k == ord('p'):
        no_points = 0
        img_grayscale = cv2.cvtColor(original_img, cv2.COLOR_BGR2GRAY)
        gauss_blur = cv2.GaussianBlur(img_grayscale, (3, 3), 1)
        ext_t = cv2.getTrackbarPos('Internal_Threshold', 'Image')
        in_t = cv2.getTrackbarPos('External_Threshold', 'Image')
        cann_img= cv2.Canny(gauss_blur, in_t,ext_t)
        
        points = []
        for i in range(cann_img.shape[0]):
            for j in range(cann_img.shape[1]):
                if cann_img[i][j] != 0:
                    points.append([i,j])
        
        #to plot the parameter space 
        for p in points:
            d_list = []
            theta_list = []
            if no_points != 10000:
                for t in range(len(theta)):
                    d = p[0]*np.cos(np.deg2rad(t))+p[1]*np.sin(np.deg2rad(t)) / 255
                    d_list.append(d)
                    theta_list.append(np.deg2rad(t))
                
                plt.plot(theta_list,d_list)
                no_points+=1
            else:
                break


        
        plt.savefig("Parameter_space.png")
        img = cv2.imread('Parameter_space.png')
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        for i in range(img.shape[0]):
            for j in range(img.shape[1]):
                img[i][j]%=255
        
        cv2.imshow("HoughParameter Space",img)

     
          


    elif k == 27: 
        break
  
# close the camera
if flag == -1:
    cap.release() 
   
# close all the opened windows 
cv2.destroyAllWindows()




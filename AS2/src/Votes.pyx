# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 16:43:40 2020

@author: SHIRISH
"""
cimport cython
import numpy as np

@cython.boundscheck(False)
@cython.wraparound(False)
def getVotes(int[:, ::1] image, dict output,int bin_size):
    
    cdef int d
    cdef int min_theta = 0
    cdef int max_theta = 180
    cdef tuple param = ()
    cdef list points = []
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            if image[i][j] != 0:
                points.append([i,j])
                
    for p in points:
        for i in range(min_theta,max_theta):
            d = int(p[0]*np.cos(np.deg2rad(i))+p[1]*np.sin(np.deg2rad(i)))
            if d >= -bin_size and d <= bin_size:
                param = (d,np.deg2rad(i))
                if param in output.keys():
                    output[param] += 1
                else:
                    output[param] = 1
                
    
    return output

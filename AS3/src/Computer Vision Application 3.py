
from keras.datasets import mnist
from keras import models
from keras import layers
from keras.utils import to_categorical
from keras import optimizers
import numpy as np
import matplotlib.pyplot as plt
import cv2 
from PIL import Image
from keras import initializers
import pickle
from keras import optimizers 
from keras.models import load_model

def PlotGraph(history):
    import matplotlib.pyplot as plt
    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = range(1,len(acc) + 1)

#Validation vs Training in accuracy
    plt.plot(epochs,acc,'bo',label='training accuracy')
    plt.plot(epochs,val_acc,'b',label='Validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.figure()

    plt.plot(epochs,loss,'bo',label='training loss')
    plt.plot(epochs,val_loss,'b',label='Validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.figure()
    plt.show()

(train_img, train_labels), (test_img, test_labels) = mnist.load_data()

#Creating new labels
for i in range(train_labels.shape[0]):
    if train_labels[i] % 2 == 0:
        train_labels[i] = 1
    else:
        train_labels[i] = 0

for i in range(test_labels.shape[0]):
    if test_labels[i] % 2 == 0:
        test_labels[i] = 1
    else:
        test_labels[i] = 0


dataset = np.concatenate((train_img, test_img))
labels = np.concatenate((train_labels,test_labels))
#Creating the model

def initial_model():
    network = models.Sequential()
    
    network.add(layers.Conv2D(32, (3,3), activation='relu',input_shape=(28,28,1)))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Conv2D(64, (3,3), activation='relu'))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Flatten())
    network.add(layers.Dense(units=64, activation='relu'))
    network.add(layers.Dense(units=32,activation='relu'))
    network.add(layers.Dropout(0.5))
    network.add(layers.Dense(1,activation='sigmoid'))
    network.summary()
    
    network.compile(optimizer='adam',loss='binary_crossentropy',metrics=['acc'])
    return network


#Reducing the number of dense layers thereby making the model simpler
def Model_different_layers():
    network = models.Sequential()
    
    network.add(layers.Conv2D(32, (3,3), activation='relu',input_shape=(28,28,1)))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Conv2D(64, (3,3), activation='relu'))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Flatten())
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units=64, activation='relu'))
    network.add(layers.Dropout(0.2))
    network.add(layers.Dense(1,activation='sigmoid'))
    network.summary()
    network.compile(optimizer='adam',loss='binary_crossentropy',metrics=['acc'])
    return network


def model_variable_param(weights,optimizer,kernel_size,strides,drop_rate,units):
    if weights == 'Xavier':
        w = initializers.GlorotNormal()
    
    elif weights == 'He':
        w = initializers.HeNormal()
        
    network = models.Sequential()
    
    network.add(layers.Conv2D(units, kernel_size, activation='relu',kernel_initializer=w,strides=strides,input_shape=(28,28,1)))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Conv2D(units * 2, kernel_size,kernel_initializer=w,activation='relu'))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Flatten())
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units*2,kernel_initializer=w, activation='relu'))
    network.add(layers.Dropout(drop_rate))
    network.add(layers.Dense(1,kernel_initializer=w,activation='sigmoid'))
    #network.summary()
    network.compile(optimizer=optimizer,loss='binary_crossentropy',metrics=['acc'])
    return network



X_train = dataset[:55000]
y_train = labels[:55000]
X_val = dataset[55000:65000]
y_val = labels[55000:65000]
X_test = dataset[65000:]
y_test = labels[65000:]


X_train = X_train.reshape((X_train.shape[0],28,28,1))
X_train = X_train.astype('float32') / 255


X_val = X_val.reshape((X_val.shape[0],28,28,1))
X_val = X_val.astype('float32') / 255

X_test = X_test.reshape((X_test.shape[0],28,28,1))
X_test = X_test.astype('float32') / 255

weights = ['Xavier','He']

kernel_size = [3,5]

strides = [1,2]

opt = ['adam','rmsprop','adagrad','SGD']


units = [8,16,32]
drop_raptes = [0.2,0.4,0.5]
results = {}



#Obtaining results from initial model
network = initial_model()
history = network.fit(X_train,y_train,epochs=30,validation_data=(X_val,y_val),batch_size=128)
PlotGraph(history)
test_loss, test_acc = network.evaluate(X_test,y_test)

#Obtaining results from Reduced Layer model
network = Model_different_layers()
history = network.fit(X_train,y_train,epochs=30,validation_data=(X_val,y_val),batch_size=128)
PlotGraph(history)

for i in weights:
    for j in kernel_size:
        for k in strides:
            for l in  opt:
                for u in units:
                    for d in drop_raptes:
                        network = model_variable_param(i, l, j, k, d, u)
                        print("Processing model for the parameter",i,j,k,l,u,d)
                        history = network.fit(X_train,y_train,epochs=10,validation_data=(X_val,y_val),batch_size=256,verbose = 0)
                        results[(i,j,k,l,u,d)] = np.mean(history.history['val_acc'])




'''
with open("Grid_search_Results", 'wb') as f:
    pickle.dump(results,f) 
'''
  
with open('Grid_search_Results', 'rb') as f:
    results = pickle.load(f)

sort_grid_search = sorted(results.items(), key=lambda x: x[1],reverse=True)

#Since its the first 
hyper_parameters = sort_grid_search[0][0]
result_lr = {}
#Searching the optimal learning rate

    

#Desigining the final model
network = model_variable_param(hyper_parameters[0], hyper_parameters[3], hyper_parameters[1], hyper_parameters[2], hyper_parameters[5], hyper_parameters[4])
network.summary()

#To check how many epochs are required we train the model for 100 epochs and then choose from the graph the epochs where the accuracy doesnt change anymore
history = network.fit(X_train,y_train,epochs=100,validation_data=(X_val,y_val),batch_size=256,verbose = 1)
PlotGraph(history)


#From the graphs we choose 30 as the number of epochs and then we retrain the model with 30 epochs
network = model_variable_param(hyper_parameters[0], hyper_parameters[3], hyper_parameters[1], hyper_parameters[2], hyper_parameters[5], hyper_parameters[4])
history = network.fit(X_train,y_train,epochs=30,validation_data=(X_val,y_val),batch_size=256,verbose = 1)
PlotGraph(history)

#Saving the final model
#network.save("MNIST_Neural_Network")
network = load_model("MNIST_Neural_Network")
test_loss, test_acc = network.evaluate(X_test,y_test)

result = network.predict(X_test)








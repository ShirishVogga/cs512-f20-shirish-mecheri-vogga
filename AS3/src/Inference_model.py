# -*- coding: utf-8 -*-
"""
Created on Sun Nov  1 15:44:29 2020

@author: SHIRISH
"""

import sys
import cv2 
from PIL import Image
import numpy as np
from keras.models import load_model


network = load_model("MNIST_Neural_Network")

if len(sys.argv) == 2:
    filename = sys.argv[1]
try:
    
    img = cv2.imread(filename)
    img_grayscale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    gauss_blur = cv2.GaussianBlur(img_grayscale, (3, 3), 1)
    
    thresholded_bin_img = cv2.adaptiveThreshold(gauss_blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, 
                                              cv2.THRESH_BINARY, 55, 5)
    
    
    
    
     
    
    #thresholded_bin_img = cv2.resize(thresholded_bin_img, (28,28),interpolation = cv2.INTER_AREA)
    thresholded_bin_img = Image.fromarray(thresholded_bin_img)
    thresholded_bin_img = thresholded_bin_img.resize((28,28))
    thresholded_bin_img = np.asarray(thresholded_bin_img)
    cv2.imshow("Original image", img)
    cv2.imshow("Binary_image", thresholded_bin_img)
    
    if cv2.waitKey(0) & 0xff == 27:  
        cv2.destroyAllWindows() 
    thresholded_bin_img = thresholded_bin_img.astype('float32') / 255
    
    thresholded_bin_img = thresholded_bin_img.reshape((thresholded_bin_img.shape[0],thresholded_bin_img.shape[1],1))
    
    thresholded_bin_img = thresholded_bin_img.reshape((1,) + thresholded_bin_img.shape)
    
    prediction = network.predict(thresholded_bin_img)
    
    if prediction[0][0] > 0.5:
        print("Even Number Detected")
    else:
        print("Odd Number Detected")
except:
    print("No input file given")
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 21:02:01 2020

@author: SHIRISH
"""


import numpy as np
import cv2 as cv
import glob
import pandas as pd
import pickle as p
import sys


#Image and object points without noise



def points_coresspondence_gen(object_file,image_file):
    img_points = []
    count = 0 
    image_file = open(image_file,mode='rb')
    
    for lines in image_file:
        if count != 0:
            temp = []
            for i in lines.split():
                temp.append(float(i))
            img_points.append(temp)
        
        count+=1
    
    img_points = np.array(img_points)
    
    
    obj_points = []
    count = 0 
    object_file = open(object_file,mode='rb')
    
    for lines in object_file:
        if count != 0:
            temp = []
            for i in lines.split():
                temp.append(float(i))
            obj_points.append(temp)
        
        count+=1
        
    obj_points = np.array(obj_points)
    
    
    points_coresspondence = np.concatenate((obj_points,img_points),axis=1)
    return points_coresspondence



if  len(sys.argv) == 1:

    #Normal images without noise
    points_coresspondence = points_coresspondence_gen("ncc-worldPt.txt", "image_points.txt")
    with open("points_coresspondence", 'wb') as f:
        p.dump(points_coresspondence,f) 
    
    #Images With noise
    points_coresspondence_noise = points_coresspondence_gen("Noisy_world.txt", "Noisy_image.txt")
    with open("points_coresspondence_noisy", 'wb') as f:
        p.dump(points_coresspondence_noise,f) 
    
    points_coresspondence_noise_2 = points_coresspondence_gen("ncc-worldPt-noise2.txt","ncc-noise-1-imagePt.txt")
    with open("points_coresspondence_noisy_2", 'wb') as f:
        p.dump(points_coresspondence_noise_2,f) 
else:
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.random.randint(300,size=(42,3))
    objp = objp.astype('float32')
    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.
    img_name = sys.argv[1]
    img = cv.imread(img_name)
    
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    # Find the chess board corners
    ret, corners = cv.findChessboardCorners(gray, (7,6), None)
    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)
        corners2 = cv.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
        imgpoints.append(corners)
        # Draw and display the corners
        cv.drawChessboardCorners(img, (7,6), corners2, ret)
        cv.imshow('img', img)
        if cv.waitKey(0) & 0xff == 27:  
            cv.destroyAllWindows() 

    
    objpoints = np.array(objpoints) 
    imgpoints = np.array(imgpoints)
    objpoints = objpoints.reshape((objpoints.shape[1],objpoints.shape[2]))
    imgpoints = imgpoints.reshape((imgpoints.shape[1],2))
    print(objpoints.shape)
    print(imgpoints.shape)
    image_points_coresspondence_file = np.concatenate((objpoints,imgpoints),axis=1)
    with open("coresspondence_image", 'wb') as f:
        p.dump(image_points_coresspondence_file,f)
    
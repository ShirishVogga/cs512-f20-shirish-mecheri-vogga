# -*- coding: utf-8 -*-
"""
Created on Sun Nov 22 13:32:16 2020

@author: SHIRISH
"""


import numpy  as np
import pickle as p
from scipy.linalg import svd
import pandas as pd
import math


    
    
def solution_space(points):
    
    x_array = np.array([points[0],points[1],points[2],1,0,0,0,0,-1*points[3]*points[0],-1*points[3]*points[1],-1*points[3]*points[2],-1*points[3]])
    y_array = np.array([0,0,0,0,points[0],points[1],points[2],1,-1*points[4]*points[0],-1*points[4]*points[1],-1*points[4]*points[2],-1*points[4]])
    x_array = x_array.reshape((1,len(x_array))).astype('float32')
    y_array = y_array.reshape((1,len(y_array))).astype('float32')
    
    result = np.concatenate((x_array,y_array))
    return result


def mag_vector(A):
    total = 0
    for i in A:
        total+=i*i
    
    return math.sqrt(total) 


def CameraProperties_gen(points):
    A = []
    
    for i in range(len(points)):
        result = solution_space(points[i])
        A.append(result)
        
    
    A = np.array(A)
    
    A = A.reshape((A.shape[0]*A.shape[1],A.shape[2]))
    
    #Removing negative zeroes
    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
            if A[i][j] == -0:
                A[i][j] = 0
    
    U, s, V = np.linalg.svd(A)
    
    
    
    projection_m = V[V.shape[0]-1]
    
    projection_m = projection_m.reshape((3,4))
    
    #To extract a1,a2,a3 matrices
    a_vectors = []
    
    for i in range(projection_m.shape[0]):
        for j in range(projection_m.shape[1]-1):
            a_vectors.append(projection_m[i][j])
            
    
    a_vectors = np.array(a_vectors).reshape((3,3))
    b_vectors = np.array([projection_m[0][3],projection_m[1][3],projection_m[2][3]])
    
    
    #TO calculate the camera extrinsic parameters
    #To calculate the unknown scale value
    
    phroe = 1 / mag_vector(a_vectors[2]) #mod(a3)
    
    #Finding u0 and v0
    u0 = math.pow(phroe,2) * np.dot(a_vectors[0],a_vectors[2])
    v0 = math.pow(phroe,2) * np.dot(a_vectors[1],a_vectors[2])
    
    #Alpha v
    temp_sum = (math.pow(phroe, 2) * np.dot(a_vectors[1], a_vectors[1])) - math.pow(v0,2)
    #print(temp_sum)
    alphaV = math.sqrt(temp_sum)
    
    #Computing scale
    
    s = (np.power(phroe, 4) * np.dot(np.cross(a_vectors[0],a_vectors[2]),np.cross(a_vectors[1],a_vectors[2])))/alphaV
    #Calculating alphau
    
    alphaU = math.sqrt(((math.pow(phroe,2)*np.dot(a_vectors[0],a_vectors[0]))-math.pow(s, 2)-math.pow(u0, 2)))
    
    
    #Constructing matrix K
    
    K = np.array([[alphaU,s,u0],[0,alphaV,v0],[0,0,1]])
    
    epsilon = np.sign(b_vectors[2])
    
    #TO calculate T*
    Tstar = epsilon * phroe * (np.dot(np.linalg.inv(K),b_vectors))
    
    #TO calculate R*
    
    r3 = epsilon * phroe * a_vectors[2]
    r1 = (math.pow(phroe,2)/alphaV)*np.cross(a_vectors[1],a_vectors[2])
    r2 = np.cross(r3,r1)
    r1 = r1.reshape((1,r1.shape[0]))
    r2= r2.reshape((1,r2.shape[0]))
    r3 = r3.reshape((1,r3.shape[0]))
    Rstar = np.concatenate((r1,r2,r3))
    
    Final_projection_matrix = phroe * projection_m

#TO calculate the fitting error or projection error
    predicted_points = []
    world_points = points[:, [0,1,2]]
    known_points = points[:, [3,4]]
    

    ones = np.ones((world_points.shape[0],1))
    
    world_points_hw = np.concatenate((world_points,ones),axis=1)
    for i in range(world_points_hw.shape[0]):
        result = np.dot(Final_projection_matrix,world_points_hw[i])
        predicted_points.append(result)
        
        
    
    #Converting to 2d points
    predicted_points = np.array(predicted_points)
    
    for i in range(predicted_points.shape[0]):
        predicted_points[i] = predicted_points[i] / predicted_points[i][2]
        
    
    points_2d = pd.DataFrame(predicted_points)
    points_2d = points_2d.iloc[:, :-1].values
    
    points_2d = np.array(points_2d).astype('float32')
    
    
    loss_mse = np.mean(np.square(np.absolute(known_points-points_2d)))
    
    
    print("Camera Parmaters ")
    print("u0 =  ",u0," v0 = ",v0)
    print("alphaU =  ",alphaU," alphaV = ",alphaV)
    print("s = ",s)
    print("T* = ",Tstar)
    print("R* = ",Rstar)
    print("Projection Matrix  M",Final_projection_matrix )
    print("Fitting error measure as mean squared error  = ",loss_mse)
    print("*************************************************************")




#Load the points_coresspondence file
try:
    with open('points_coresspondence', 'rb') as f:
        points = p.load(f)
    
    with open('points_coresspondence_noisy', 'rb') as f:
        points_noisy = p.load(f)
    
    with open('points_coresspondence_noisy_2', 'rb') as f:
        points_noisy_2 = p.load(f)
    
    print("Camera Properties for Normal image")    
    CameraProperties_gen(points)

    print("Camera properties for Noisy image")
    CameraProperties_gen(points_noisy)

    print("Camera properties for Noisy image 2")
    CameraProperties_gen(points_noisy_2)
except:
    print("File not generated ")

#Load the points_coresspondence file

try:
    with open('coresspondence_image', 'rb') as f:
        points_image = p.load(f)
    
    print("Camera properties for custom loaded image")
    CameraProperties_gen(points_image)

except:
    print("File not found")
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 10:06:58 2020

@author: SHIRISH
"""


import numpy as np

import pickle as p
import math
import pandas as pd
with open('points_coresspondence', 'rb') as f:
    points = p.load(f)
    
    
def solution_space(points):
    
    x_array = np.array([points[0],points[1],points[2],1,0,0,0,0,-1*points[3]*points[0],-1*points[3]*points[1],-1*points[3]*points[2],-1*points[3]])
    y_array = np.array([0,0,0,0,points[0],points[1],points[2],1,-1*points[4]*points[0],-1*points[4]*points[1],-1*points[4]*points[2],-1*points[4]])
    x_array = x_array.reshape((1,len(x_array))).astype('float32')
    y_array = y_array.reshape((1,len(y_array))).astype('float32')
    
    result = np.concatenate((x_array,y_array))
    return result


def mag_vector(A):
    total = 0
    for i in A:
        total+=i*i
    
    return math.sqrt(total)



def fit(points):
    A = []

    for i in range(len(points)):
        result = solution_space(points[i])
        A.append(result)
        
    
    A = np.array(A)
    
    A = A.reshape((A.shape[0]*A.shape[1],A.shape[2]))
    
    #Removing negative zeroes
    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
            if A[i][j] == -0:
                A[i][j] = 0
    
    U, s, V = np.linalg.svd(A)

        
    projection_m = V[V.shape[0]-1]
    
    projection_m = projection_m.reshape((3,4))
    
    #To extract a1,a2,a3 matrices
    a_vectors = []
    
    for i in range(projection_m.shape[0]):
        for j in range(projection_m.shape[1]-1):
            a_vectors.append(projection_m[i][j])
            
    
    a_vectors = np.array(a_vectors).reshape((3,3))
    b_vectors = np.array([projection_m[0][3],projection_m[1][3],projection_m[2][3]])
    
    #To calculate the unknown scale value
    
    phroe = 1 / mag_vector(a_vectors[2])
    
    final_projection_matrix = phroe * projection_m
    
    return final_projection_matrix


def loss_mse(projection_m,labels,points):
    ones = np.ones((points.shape[0],1))
    predicted_points = []
    world_points = points[:,[0,1,2]]
    world_points_hw = np.concatenate((world_points,ones),axis=1)
    #print(world_points_hw.shape[0])
    for i in range(world_points_hw.shape[0]):
        result = np.dot(projection_m,world_points_hw[i])
        predicted_points.append(result)
        
        
    
    #Converting to 2d points
    predicted_points = np.array(predicted_points)
    
    for i in range(predicted_points.shape[0]):
        predicted_points[i] = predicted_points[i] / predicted_points[i][2]
        
    
    points_2d = pd.DataFrame(predicted_points)
    points_2d = points_2d.iloc[:, :-1].values
    
    points_2d = np.array(points_2d).astype('float32')
    
    
    loss_mse = np.square(np.absolute(labels-points_2d))
    return loss_mse

def RansacIMPL(points):
#Extracting the ransack config parameters
    ransac_config = open("RANSAC Config.txt",mode='rb')
    config = []
    for lines in ransac_config:
        config.append(float(lines.strip()))
        
    
    n_samples = int(config[0])
    d = int(config[1])
    k = int(config[2])
    p = config[3]
    t = config[4]
    w = 0.5
    inliers = []
    for i in range(k):
        #print("Processing Fold ",i)
        batch_readings = []
        random_indices = np.random.choice(points.shape[0],size=n_samples,replace=True)
        sample_points = points[random_indices, :]
        labels = sample_points[:,[3,4]]
        projection_m = fit(sample_points)
        loss = loss_mse(projection_m, labels, sample_points)  
        #print(loss)
        #Checking for inliers
        for i in range(loss.shape[0]):
            if np.sum(loss[i]) <= t:
                 batch_readings.append(sample_points[i])
        
        if len(batch_readings) >= d:
            inliers.append(batch_readings)
            #print(w)
            k_estimate = (np.log10(1-p)/np.log10(1 - math.pow(w, n_samples)))
            w = len(batch_readings)/n_samples
        else:
            #discard the inliers found for the batch
            pass
        
        t = 1.5 * np.median(loss)    
        
        
    #Extracting the inliers from the list of lists to a numpy array
        
    inliers_list = []
    for i in range(len(inliers)):
        temp = np.array(inliers[i])
        for j in range(temp.shape[0]):
            inliers_list.append(temp[j])
            
        
    inliers_list = np.array(inliers_list)
    
    #Refitting the model
    labels_inliers = inliers_list[:, [3,4]] 
    projection_matrix_RANSAC = fit(inliers_list) 
    loss_inliers = np.mean(loss_mse(projection_matrix_RANSAC, labels_inliers, inliers_list))  
    
    #Printing the RANSAC parameters such as k,w,t,loss_mse for Ransac implementation
    
    print("K expiriments estimate = ",k_estimate)
    print("Proability a point is an inlier = ",w)
    print("Threshold Value t = ",t)
    print("Mean Squared error using RANSAC = ",loss_inliers)
    print("****************************************************************")
    
    
    
    
try:
    with open('points_coresspondence', 'rb') as f:
        points = p.load(f)
    
    with open('points_coresspondence_noisy', 'rb') as f:
        points_noisy = p.load(f)
    
    with open('points_coresspondence_noisy_2', 'rb') as f:
        points_noisy_2 = p.load(f)
    
    print("Ransac for Image without Noise")
    RansacIMPL(points)
    
    print("Ransac implementation for Images with noise ")
    RansacIMPL(points_noisy) 
    
    print("Ransac implementation for Images with noise 2 ")
    RansacIMPL(points_noisy_2) 

except:
    print("File not generated ")

#Load the points_coresspondence file

try:
    with open('coresspondence_image', 'rb') as f:
        points_image = p.load(f)
    
    print("RANSAC Implementation for a image")
    RansacIMPL(points_image)   

except:
    print("File not found")




    
    
    
    
    
    
    
    
tqdm
opencv-python
Pillow
tensorflow-gpu==2.3.0
scikit-learn
scipy
pandas
numpy
